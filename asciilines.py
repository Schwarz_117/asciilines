import sys

def changeCanvas(canvas, rend, rowP, colP, HorV, lineLen):
    if int(rowP) > len(canvas) or int(colP) > len(canvas):
        return
    elif HorV == 'h':
        x = int(colP)
        y = int(rowP)
        if y < 0:
            return
        while x < int(lineLen) + int(colP) and x < len(canvas[0]):
            canvas[y][x] = rend
            x += 1
    else:
        x = int(colP)
        y = int(rowP)
        if x < 0:
            return
        while y < int(lineLen) + int(rowP) and y < len(canvas):
            canvas[y][x] = rend
            y+= 1



f = open(sys.argv[1], 'r')
cat = f.read()
f.close()

lines = cat.split()

rows = int(lines[0])
cols = int(lines[1])

del lines[0]
del lines[0]

canvas = [['.'] * cols for i in range(rows)]

var = len(lines) / 5

x = 0
while x < var:
    rend = lines[0]
    del lines[0]
    rowP = lines[0]
    del lines[0]
    colP = lines[0]
    del lines[0]
    HorV = lines[0]
    del lines[0]
    lineLen = lines[0]
    del lines[0]
    x += 1
    changeCanvas(canvas, rend, rowP, colP, HorV, lineLen);

j = 0

while j < len(canvas):
    print(*canvas[j], sep='')
    j += 1

#print(*canvas[0], sep='')
#print(*canvas[1], sep='')
#print(*canvas[2], sep='')





#x = 0
#while x < rows:
#    y = 0
#    while y < cols:
#        print('.', end='')
#        y+= 1
#    print('')
#    x += 1
