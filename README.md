# Ascii Lines

Copyright (c) 2019 Cole Schwarz

This program takes .tvg files and renders the file contents as ASCII on standard output.

# Build and Run

This is a python program and to run you will need to use the python3 command in sheel to execute.

## Example

$ python3 asciilines.py test/test1.tvg<br/>
.#..<br/>
*#**<br/>
.#..<br/>

# License

This program is licensed under the "MIT License". Please see the file LICENSE in the source distribution of this software for license terms.


